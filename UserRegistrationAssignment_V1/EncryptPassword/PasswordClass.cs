﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace UserRegistrationAssignment_V1
{
    public class PasswordClass
    {
        public static string Encrypt(string password)
        {
            UnicodeEncoding unicodeEncode = new UnicodeEncoding();
            byte[] bytPassword = unicodeEncode.GetBytes(password);
            SHA512Managed sha = new SHA512Managed();
            byte[] hash = sha.ComputeHash(bytPassword);
            return Convert.ToBase64String(hash);
        }
    }
}