﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace UserRegistrationAssignment_V1.ViewModels
{
    public class ChangePassword
    {
        public class UserLogin
        {
            [Required]
            [Display(Name = "Email")]
            [EmailAddress]
            public string EmailId { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [Required(ErrorMessage = "You must provide a password")]
            [DataType(DataType.Password)]
            [Display(Name = "New Password")]
            public string NewPassword { get; set; }

            [Required(ErrorMessage = "You must provide the same password")]
            [DataType(DataType.Password)]
            [Compare("NewPassword", ErrorMessage = "The password and confirmation password do not match.")]
            [Display(Name = "Confirm New Password")]
            public string ConfirmPassword { get; set; }
        }
    }
}