namespace UserRegistrationAssignment_V1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserContacts",
                c => new
                    {
                        HoldsId = c.Int(nullable: false, identity: true),
                        ContactNumber = c.String(),
                        ContactId = c.Int(nullable: false),
                        Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.HoldsId)
                .ForeignKey("dbo.UserContactTypes", t => t.ContactId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.Id, cascadeDelete: true)
                .Index(t => t.ContactId)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.UserContactTypes",
                c => new
                    {
                        ContactId = c.Int(nullable: false, identity: true),
                        Type = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ContactId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                        Address = c.String(),
                        IsEmailVarified = c.Boolean(nullable: false),
                        ActivationCode = c.Guid(nullable: false),
                        Mobile = c.String(),
                        ImageUrl = c.String(),
                        AlternetText = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserContacts", "Id", "dbo.Users");
            DropForeignKey("dbo.UserContacts", "ContactId", "dbo.UserContactTypes");
            DropIndex("dbo.UserContacts", new[] { "Id" });
            DropIndex("dbo.UserContacts", new[] { "ContactId" });
            DropTable("dbo.Users");
            DropTable("dbo.UserContactTypes");
            DropTable("dbo.UserContacts");
        }
    }
}
